<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
	
	public function submit(Request $request)
	{
		$kategori = $request->input('nama_kategori');
		$kategori_all = json_decode($request->kategori,TRUE);
		$kategori_all = array_merge($kategori,$kategori_all);
		return view('template',['data' => $kategori_all]);
	}

	public function edit(Request $request)
	{
		$kategori_all = json_decode($request->kategori,TRUE);
		$key = $request->key;
		$value = $request->nama_kategori;
		$kategori_all[$key] = $value;
		return view('template',['data' => $kategori_all]);
	}

	public function delete(Request $request, $key)
	{
		$kategori_all = json_decode($request->data,true);
		unset($kategori_all[$key]);
		return view('template',['data' => $kategori_all]);
	}
}