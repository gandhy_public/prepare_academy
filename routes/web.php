<?php

Route::get('/', function () {
    return view('template', ['data' => []]);
});
Route::get('/gandhy', function(){
	return "Hello Gandhy";
});
Route::get('/helo/{nama}', function($nama){
	return "Hello $nama";
});
Route::post('kategori', 'KategoriController@submit');
Route::post('kategori/edit/', 'KategoriController@edit');
Route::get('kategori/delete/{key}', 'KategoriController@delete');