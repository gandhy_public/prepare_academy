  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contoh Form dan Tabel
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="/kategori" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label>Nama Kategori</label>
                  <input type="text" name="nama_kategori[]" class="form-control" placeholder="Masukkan nama kategori produk">
                  <input type="hidden" name="kategori" value="{{ json_encode($data,TRUE) }}">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Tabel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Action</th>
                </tr>
                @foreach($data as $key => $value)
                <tr>
                  <td>{{ $key }}</td>
                  <td>{{ $value }}</td>
                  <td>
                    <button type="button" class="btn btn-primary btn-edit" data-kategori="{{ $value }}" data-id="{{ $key }}">Edit</button>
                    <a href="/kategori/delete/{{$key}}?data={{json_encode($data,TRUE)}}"><button type="button" class="btn btn-danger">Delete</button></a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="kategori/edit">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" name="nama_kategori" class="form-control" placeholder="Masukkan nama kategori produk" id="nama_kategori">
            <input type="hidden" name="kategori" value="{{ json_encode($data,TRUE) }}">
            <input type="hidden" name="key" id="key">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
@include('base.footer')

<script type="text/javascript">
  $('.btn-edit').on('click', function() {
      $('#modal-default').modal('show');
      $('#nama_kategori').val($(this).data('kategori'));
      $('#key').val($(this).data('id'));
  });
</script>
<!-- text -->